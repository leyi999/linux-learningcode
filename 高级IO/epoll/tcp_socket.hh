#include <iostream>
#include <cstring>
#include <cerrno>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <unistd.h>
// socket
static int get_socket_fd(const uint16_t port)
{
    // socket
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        return -1;
    }
    // info
    sockaddr_in my_addr;
    bzero(&my_addr, sizeof(sockaddr_in));
    my_addr.sin_family = AF_INET;
    my_addr.sin_addr.s_addr = INADDR_ANY;
    my_addr.sin_port = htons(port);
    // bind
    if (bind(socket_fd, (sockaddr *)&my_addr, sizeof(sockaddr_in)) == -1)
    {
        return -2;
    }
    return socket_fd;
}
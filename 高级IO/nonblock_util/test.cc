#include "util.hh"
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cerrno>
using namespace std;
int main()
{
    // 设置描述符为非阻塞
    util_t::set_fd_nonblock(0);
    char buffer[1024];
    bzero(buffer, sizeof buffer);
    cout << "start" << endl;
    while (true)
    {
        if (scanf("%s", buffer) == EOF && errno == EWOULDBLOCK)
            cout << "资源不就绪 正在做其他事情" << endl;
        else
            cout << "#inputed:: " << buffer << endl;
        sleep(1);
    }
    return 0;
}
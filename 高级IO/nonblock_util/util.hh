#pragma once
#include <fcntl.h>
#include <unistd.h>
class util_t
{
public:
    // 设置文件描述符 非阻塞
    static bool set_fd_nonblock(int fd)
    {
        // 获取当前文件描述符状态(位图形式)
        int flag = fcntl(fd, F_GETFL);
        if (flag == -1)
            return false;
        // 设置非阻塞
        if (fcntl(fd, F_SETFL, flag | O_NONBLOCK) == -1)
            return false;
        return true;
    }
};
#include <iostream>
#include "servce.hh"
bool request_handler(connection_t *p_con)
{
    servce_handler(p_con, calculate);
    return true;
}
int main()
{
    reactor_server_t server(request_handler, 8090, 5);
    if (!server.run())
    {
        strerror(errno);
        exit(1);
    }

    return 0;
}
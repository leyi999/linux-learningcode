#pragma once
#include <unistd.h>
#include <sys/epoll.h>
class epoll_t
{
public:
    epoll_t(int timeout, int list_len = 128)
        : _epoll_fd(epoll_create1(0)), _timeout(timeout), _list_len(list_len)
    {
        //开辟就绪事件列表来拷贝epoll模型里的就绪队列
        _ready_list = new epoll_event[list_len];
    }
    ~epoll_t()
    {
        delete[] _ready_list;
    }
    bool modify(int fd, uint32_t events)
    {
        epoll_event evt;
        evt.data.fd = fd;
        evt.events = events;
        return epoll_ctl(_epoll_fd, EPOLL_CTL_MOD, fd, &evt) == 0;
    }
    bool add(int fd, uint32_t events)
    {
        epoll_event evt;
        evt.data.fd = fd;
        evt.events = events;
        return epoll_ctl(_epoll_fd, EPOLL_CTL_ADD, fd, &evt) == 0;
    }
    int wait()
    {
        return epoll_wait(_epoll_fd, _ready_list, _list_len, _timeout);
    }
    epoll_event *get_event(int index)
    {
        if (index < 0 || index >= _list_len)
            return nullptr;
        return _ready_list + index;
    }
    bool del(int fd)
    {
        return epoll_ctl(_epoll_fd, EPOLL_CTL_DEL, fd, nullptr) == 0;
    }

private:
    int _epoll_fd;
    int _timeout;
    epoll_event *_ready_list;
    int _list_len;
};
#pragma once
#include"log.hpp"
#include<cstring>
#include<cstdio>
#include<cerrno>
#include<iostream>
#include<unistd.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/stat.h>
#include<sys/fcntl.h>
#include<sys/shm.h>
#define key_path "./"
#define key_id 06
#define shm_size 4096
//利用管道来控制不安全的临界资源
#define pipe_path "./.ipc_pipe"
#define pipe_for_client O_WRONLY
#define pipe_for_server O_RDONLY
int get_pipe(int mode,const char*path=pipe_path ){
    int mk_ret=mkfifo(pipe_path,0777);
    if(mk_ret==-1&&errno!=EEXIST)
        return errno;
    return open(pipe_path,mode);
}
int ipc_wait(int pipe_fd){
    int sig=0;
    //在管道里阻塞等待信号
    ssize_t read_ret=read(pipe_fd,&sig,sizeof(int));
    return sig;
}
int ipc_signal(int pipe_fd,int sig=1){
    ssize_t write_ret=write(pipe_fd,&sig,sizeof(int));
    return sig;
}
key_t creat_key(const char* path= key_path,int id =key_id){
    key_t key= ftok(path,id);
    if(key==0){
        log()<<"生成key出错"<<std::endl;
        exit(1);
    }
    return key;
}
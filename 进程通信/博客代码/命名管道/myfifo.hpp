#pragma once
#include<assert.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/fcntl.h>
#include<errno.h>
#include<stdio.h>
//文件路径 隐藏文件
#define fifo_path "./.myfifo"
#define fifo_mode 0666
//缓冲区大小
#define buffer_size 128
#include"myfifo.hpp"
int main(){
    //创建命名管道
    int mkfifo_ret=mkfifo(fifo_path,fifo_mode);
    //判断是否存在
    if(mkfifo_ret==-1&&errno!=EEXIST)
        return errno;
    //打开管道 只读
    int myfifo_fd= open(fifo_path,O_RDONLY);
    char server_buffer[buffer_size];
    //从管道接收数据
    while(1){
        ssize_t read_ret=read(myfifo_fd,server_buffer,buffer_size-1);
        if(read_ret==0)
            break;
        //保证安全性
        assert(read_ret<=buffer_size);
        //输出到标准输出
        printf("服务器收到:> %s",server_buffer);
    }
    close(myfifo_fd);
    return 0;
}
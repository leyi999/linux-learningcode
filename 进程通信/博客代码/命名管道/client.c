#include"myfifo.hpp"
int main(){
    //创建命名管道
    int mkfifo_ret=mkfifo(fifo_path,fifo_mode);
    //判断是否存在
    if(mkfifo_ret==-1&&errno!=EEXIST)
        return errno;
    //打开管道 只写
    int myfifo_fd= open(fifo_path,O_WRONLY);
    char client_buffer[buffer_size];
    while(1){
        //从标准输入读取数据
        printf("请输入:> ");
        fflush(stdout);
        ssize_t read_ret=read(0,client_buffer,buffer_size-1);
        //保证安全性
        assert(read_ret<buffer_size);
        client_buffer[read_ret]=0;
        //像管道写入
        write(myfifo_fd,client_buffer,read_ret+1);
        if(strcmp(client_buffer,"_exit\n")==0)
            break;
    }
    //关闭写端 server会读取到EOF
    close (myfifo_fd);
    return 0;
}
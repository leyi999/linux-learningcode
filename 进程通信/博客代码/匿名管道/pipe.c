#include<string.h>
#include<stdlib.h>
#include<errno.h>
#include<assert.h>
#include<stdio.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<unistd.h>

#define buffer_size (128)
int main(){
    int fd[2]={0,0};
    int pipe_ret=pipe(fd); 
    if(pipe_ret) return errno;//调用出错返回错误码
    pid_t child_pid=fork();
    if(child_pid==-1) return errno;
    if(child_pid==0){
        //child
        //关闭不需要的端口
        close(fd[0]);
        char child_buffer[buffer_size];
        while(1){
            //从键盘读入数据
            ssize_t read_size=read(0,child_buffer,buffer_size-1);
            //保证安全性
            assert(read_size<buffer_size);  
            child_buffer[read_size]=0;
            //结束条件
            if(strcmp(child_buffer,"_exit\n")== 0)
                break;
            //发送给父进程
            write(fd[1],child_buffer,read_size+1);
        }
        close(fd[1]);
        exit(0);
    }
    else{
        //father
        close(fd[1]);
        char father_buffer[buffer_size];       
        while(1){
            //从管道读取数据
            ssize_t read_size=read(fd[0],father_buffer,buffer_size-1);
            //判断是否为文件结尾（子进程关闭写端）
            if(read_size==0)
                break;
            //保证安全性
            assert(read_size<=buffer_size);
            //输出内容  
            printf("父进程收到：%s",father_buffer);
        }
        close(fd[0]);
        //回收子进程
        pid_t wait_ret=waitpid(child_pid,(int*)0,0);
        if(wait_ret==-1) return errno;       
    }

    return 0;
}
#include"socket.hh"
#include<iostream>
using namespace std;
int main(){
    //如需绑定80号端口 需要root权限 使用sudo
    tcp_socket_t tcp_socket(80);
    cout<<"tcp_socket_fd: "<<tcp_socket.get_fd()<<" port: "<<tcp_socket.get_port()<<endl;
    tcp_socket_t udp_socket(8092);
    cout<<"udp_socket_fd: "<<udp_socket.get_fd()<<" port: "<<udp_socket.get_port()<<endl;
    return 0;
}

#include <cassert>
#include "server.hh"
#include "daemonize.hh"
using namespace std;
void usage()
{
    cout << "usage:> ./server.out (addr) port " << endl;
    cout << "     example: 1> ./server.out xxx.xx.xx.xx 8080" << endl;
    cout << "              2> ./server.out 8081" << endl;
}
int main(int argc_num, char *argv[])
{
    if (argc_num != 2 && argc_num != 3)
    {
        usage();
        return 0;
    }
    //成为守护进程
    int dae_ret = daemonize();
    assert(dae_ret > 0);
    //初始化服务器
    string addr = (argc_num == 3 ? argv[1] : "");
    uint16_t port = (atoi(argv[argc_num == 3 ? 2 : 1]));
    tcp_server_t tcp_server(addr, port);
    //运行
    tcp_server.run();
    return 0;
}
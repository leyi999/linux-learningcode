#include <cstdio>
#include<iostream>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
using namespace std;
int daemonize(){
    pid_t id =fork();
    //忽略SIGPIPE 防止先客户端写入时,客户端已经退出(fd被关闭)时收到信号终止服务
    signal(SIGPIPE, SIG_IGN);
    //不让自己是组长
    if(id)exit(0);
    //独立会话
    int set_ret=setsid();
    if(set_ret==-1)return set_ret;
    //打开垃圾桶
    int open_ret=open("/dev/null",O_RDWR);
    if(open_ret<0)return open_ret;
    cout<<"服务已部署..."<<endl;
    //重定向
    dup2(open_ret,0);
    dup2(open_ret,1);
    dup2(open_ret,2);
    close(open_ret);
    return 0;
}
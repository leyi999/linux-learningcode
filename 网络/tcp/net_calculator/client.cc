#include"client.hh"
using namespace std;
void usage()
{
    cout << "usage:> ./client.out server_addr server_port " << endl;
    cout << "     example: 1> ./client.out xxx.xx.xx.xx 8080" << endl;
}
int main(int argc_num, char *argv[])
{
    if (argc_num != 3)
    {
        usage();
        return 0;
    }
    string server_addr(argv[1]);
    uint16_t server_port=atoi(argv[2]);
    //初始化客户端
    tcp_client_t client(server_addr,server_port);
    //连接至服务器
    client.connect_to_server();
    //交互
    client.interact();

    return 0;
}
#include<iostream>

#define SOCKET_CREAT_ERROR 1
#define BIND_ERROR 2
#define LISTEN_ERROR 3
#define ACCEPT_ERROR 4
#define THREAD_CREATE_ERROR 5
#define CONNECT_TO_SERVER_ERROR 6
#define POPEN_ERROR 7
using namespace std;

void log_error(const char *error_str, int exit_code)
{
    cerr << error_str << endl;
    exit(exit_code);
}
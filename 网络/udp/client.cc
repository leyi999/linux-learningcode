#include <iostream>
#include <cstring>
#include <string>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
using namespace std;
void usage()
{
    cout << "./client.out to_ip to_port" << endl;
}
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        usage();
        return 1;
    }
    //打开插座
    int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_fd == -1)
    {
        cerr << strerror(errno);
        return 0;
    }
    cout << "open socket succeed...";
    //创建套接字
    struct sockaddr_in peer;
    bzero(&peer, sizeof(peer));
    peer.sin_family = AF_INET;
    //这里不能转
    peer.sin_addr.s_addr = inet_addr(argv[1]);
    cout<<peer.sin_addr.s_addr<<endl;
    peer.sin_port = htons(atoi(argv[2]));
    cout<<"ip:"<<peer.sin_addr.s_addr<<" prot:"<<peer.sin_port<<endl;
    while (1)
    {
        string send_str("");
        cout << "please enter:> ";
        cin >> send_str;
        send_str+="\n";
        int send_size=sendto(socket_fd, send_str.c_str(), send_str.size() + 1, 0, (struct sockaddr *)&peer, sizeof(peer));
    }
    return 0;
}
#include<iostream>
#include<unistd.h>
#include<sys/types.h>
#include<signal.h>
using namespace std;
void handler(int sig){
    cout<<"接收到 "<<sig<<"号信号"<<endl;
    while(1){
        cout<<"handling pid: "<<getpid()<<endl;
        sleep(2);
    }
}
int main(){
    struct sigaction action;
    //设为默认
    action.sa_flags=0;
    action.sa_restorer=nullptr;
    action.sa_sigaction=nullptr;
    //自定义行为
    action.sa_handler=handler;
    //在处理2号信号时 阻塞其他所有信号
    sigset_t block_set;
    sigfillset(&block_set);
    action.sa_mask=block_set;
    //与2号信号绑定
    sigaction(2,&action,nullptr);//不需要获取曾经的action
    while(1){
        cout<<"Runing Pid: "<<getpid()<<endl;
        sleep(2);
    }
}

#include<iostream>
#include<unistd.h>
#include<signal.h>
using namespace std;
void hander(int sig){
    cout<<"接收到信号： "<<sig<<endl;
}
int main(){
    //信号就是让OS去把PCB里的相关属性修改 一般收到信号都会终止进程
    //键盘也可以向前台进程发信号（通过阻断告知cpu后os执行操作） ctl+c 2 ctl+/ 3 ctl+z 20 ... 
    //自定义行为 否则就是默认行为
    signal(SIGTSTP/*20*/,hander);
    signal(2/*SIGINT*/,hander);
    signal(SIGALRM,hander);
    signal(9/*SIGKILL*/,hander);//无法捕捉的信号 
    signal(SIGABRT/*6*/,hander);
    //abort();//进程自身也可以产生信号//无论如何都退出的信号 ，这个与9不同 它会先执行自定义行为(如果有)，如果自定义行为没有结束进程，再退出
        alarm(2);//时间到了收到14 信号 
        //*((char*)0)=100;内存管理单元 mmu 发现页表没有 转换失败也会产生信号，os知道后设置信号
        //int a= 2/0 cpu运算时检查出错误，产生信号。 这些都是硬件问题导致的信号
        //1 指令 2.系统调用 2 键盘 3 进程自身条件（本质上是对系统调用的封装） 4.硬件问题本质上都是 告诉os去修改PCB中的信号位图结构
    //kill(getpid(),9); 进程自杀
    while(1){
        cout<<"hehe mypid: "<<getpid()<<endl;
        sleep(1);
        abort();//调用abort 产生6号信号正常调用hander表里的hander但是若hander没有终止进程 它会强制终止进程
    }
    return 0;
}
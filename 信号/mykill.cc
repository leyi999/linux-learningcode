#include<cstdlib>
#include<iostream>
#include<cerrno>
#include<cstring>
#include<sys/types.h>
#include<signal.h>
using namespace std;
void useage(){
    cout<<"useage:: mykill sig pid"<<endl;
}
int main(int argc ,char* argv[]){
    if(argc!=3){
        useage();
        exit(2);
    }
    if(kill((pid_t)atoi(argv[2]),atoi(argv[1]))){
        cout<<strerror(errno)<<endl;
        return errno;
    }
    return 0;
}
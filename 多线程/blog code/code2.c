//头文件 pthread.h
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
void *thread_routine(void *argc)
{
    printf("%s : start running\n", (char *)argc);
    // other work
    sleep(1);
    // exit
    printf("exit\n");
    //退出方式1
    // return NULL;
    //退出方式2
    pthread_exit(NULL);
}
int main()
{
    pthread_t tid_1, tid_2, tid_3;
    pthread_create(&tid_1, NULL, thread_routine, (void *)"thread_1");
    pthread_create(&tid_2, NULL, thread_routine, (void *)"thread_2");
    pthread_create(&tid_3, NULL, thread_routine, (void *)"thread_3");
    void *thread_ret;                 //输出型参数
    pthread_join(tid_1, &thread_ret); //阻塞等待
    printf("join succeed return value:%d\n", (long int)thread_ret);
    pthread_join(tid_2, &thread_ret); //阻塞等待
    printf("join succeed return value:%d\n", (long int)thread_ret);
    pthread_join(tid_3, &thread_ret); //阻塞等待
    printf("join succeed return value:%d\n", (long int)thread_ret);
    return 0;
}
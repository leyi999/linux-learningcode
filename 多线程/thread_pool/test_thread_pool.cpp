#include "thread_pool.hpp"
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <unordered_map>
#include <functional>
#include <unistd.h>
using namespace std;
//获取方法
function<void()> get_task(int a, int b, char op)
{
    if (op == '+')
        return [=]()
        { cout << a << op << b << '=' << a + b << endl; };
    if (op == '-')
        return [=]()
        { cout << a << op << b << '=' << a - b << endl; };
    if (op == '*')
        return [=]()
        { cout << a << op << b << '=' << a * b << endl; };
    if (op == '/')
        return [=]()
        {
            if (b)
                cout << a << op << b << '=' << a / b << endl;
            else
                cerr << "zero divide error!" << endl;
        };
}

int main()
{
    //随机数模拟需求
    srand(static_cast<unsigned int>(time(static_cast<time_t *>(nullptr))) ^ getpid() ^ pthread_self());
    //生成线程池
    thread_pool_t<function<void()>>* p_thread_pool=thread_pool_t<function<void()>>::get_thread_pool_instance(6);
    //启动线程池
    p_thread_pool->start();
    //主线程派发任务
    while (1)
    {
        int a = rand() % 11;
        char op = "+-*/"[rand() % 4];
        int b = rand() % 11;
        cout <<"主线程派发任务:"<< a << op << b << "=?" << endl;
        p_thread_pool->push(get_task(a, b, op));
        //sleep(1);
    }
    return 0;
}
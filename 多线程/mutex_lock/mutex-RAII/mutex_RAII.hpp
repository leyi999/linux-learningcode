// code 5 mutex + RAII
#include <string>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
using namespace std;
class tickets
{
public:
    tickets(size_t num)
        : _tickets(num) { ; }
    size_t get_ticket()
    {
        if (_tickets)
        {
            return _tickets--;
        }
        return 0;
    }
private:
    int _tickets;
};
struct thread_acgcs
{
    thread_acgcs(string &name, pthread_mutex_t *pmutex)
        : _name(name), _pmutex(pmutex) { ; }

    thread_acgcs(string &&name, pthread_mutex_t *pmutex)
        : _pmutex(pmutex)
    {
        _name.swap(name);
    }
    string _name;
    pthread_mutex_t *_pmutex;
};
//RAII
class smart_mutex
{
public:
    smart_mutex(pthread_mutex_t *pmutex)
        : _pmutex(pmutex)
    {
        pthread_mutex_lock(_pmutex);
    }
    ~smart_mutex()
    {
        pthread_mutex_unlock(_pmutex);
    }

private:
    pthread_mutex_t *_pmutex;
};
